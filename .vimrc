nnoremap <leader>aq :w<cr>:AsyncRun tmux send-keys -t 0 C-c Enter "elm-make Main.elm --warn && elm-reactor" Enter<cr>
nnoremap <leader>ad :w<cr>:AsyncRun tmux send-keys -t 0 C-c Enter "elm-make Main.elm --warn --debug && elm-reactor" Enter<cr>
" Abolish {M,m}atric {}atrix
" Abolish {H,h}eigth {}eight
