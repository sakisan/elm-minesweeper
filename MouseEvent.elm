module MouseEvent exposing (rightClick)

import Json.Decode as Json
import VirtualDom


type alias Position =
    { x : Int
    , y : Int
    }


offsetPosition : Json.Decoder Position
offsetPosition =
    Json.map2 Position (Json.field "pageX" Json.int) (Json.field "pageY" Json.int)


mouseEvent : String -> (Position -> msg) -> VirtualDom.Property msg
mouseEvent event messager =
    VirtualDom.onWithOptions event
        { preventDefault = True, stopPropagation = True }
        (Json.map messager offsetPosition)


rightClick : (Position -> msg) -> VirtualDom.Property msg
rightClick =
    mouseEvent "contextmenu"
