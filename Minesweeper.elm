module Minesweeper
    exposing
        ( Cell
        , CellState(..)
        , Config
        , Grid
        , Location
        , get
        , getNumber
        , isLost
        , isWon
        , numberOfFlags
        , mark
        , newGrid
        , reveal
        , revealAll
        , revealIfUntouched
        , square
        , toListWithLocation
        )

import Matrix exposing (Matrix)
import Maybe.Extra as MaybeExtra
import Utils exposing (..)


type alias Location =
    Matrix.Location


type alias Grid =
    Matrix Cell


type alias Config =
    { rows : Int
    , columns : Int
    , mines : Int
    }


type alias Cell =
    { mine : Bool
    , state : CellState
    }


type CellState
    = Untouched
    | Flag
    | Question
    | Revealed


square : Int -> Grid
square size =
    Matrix.square size (\_ -> { mine = False, state = Untouched })


newGrid : Config -> List Location -> Grid
newGrid config locations =
    Matrix.matrix config.rows
        config.columns
        (\location ->
            { mine = List.member location locations
            , state = Untouched
            }
        )


withStateRevealed : Cell -> Cell
withStateRevealed cell =
    { cell | state = Revealed }


isLost : Grid -> Bool
isLost matrix =
    matrix
        |> Matrix.flatten
        |> List.any (\cell -> cell.mine && cell.state == Revealed)


isWon : Grid -> Bool
isWon matrix =
    matrix
        |> Matrix.flatten
        |> List.filter (not << .mine)
        |> List.all (\cell -> cell.state == Revealed)


numberOfFlags : Grid -> Int
numberOfFlags matrix =
    matrix
        |> Matrix.flatten
        |> List.filter (\cell -> cell.state == Flag)
        |> List.length


mark : Location -> Grid -> Grid
mark location matrix =
    let
        cycleMark : Cell -> Cell
        cycleMark cell =
            { cell
                | state =
                    case cell.state of
                        Untouched ->
                            Flag

                        Flag ->
                            Question

                        Question ->
                            Untouched

                        Revealed ->
                            Revealed
            }
    in
    Matrix.update location cycleMark matrix


revealIfUntouched : Location -> Grid -> Grid
revealIfUntouched location matrix =
    let
        shouldReveal : Grid -> Bool
        shouldReveal grid =
            get location grid
                |> Maybe.map (\cell -> cell.state == Untouched)
                |> Maybe.withDefault False
    in
    matrix
        |> when shouldReveal (reveal location)


reveal : Location -> Grid -> Grid
reveal location matrix =
    case Matrix.get location matrix of
        Just cell ->
            case cell.state of
                Revealed ->
                    matrix

                _ ->
                    revealNoPropagation location matrix
                        |> when (isZero location)
                            (revealSurroundings location)

        Nothing ->
            matrix


revealNoPropagation : Location -> Grid -> Grid
revealNoPropagation location matrix =
    Matrix.update location withStateRevealed matrix


revealSurroundings : Location -> Grid -> Grid
revealSurroundings center matrix =
    let
        updater : ( Location, Cell ) -> (Grid -> Grid)
        updater ( location, _ ) =
            reveal location
    in
    getSurroundings center matrix
        |> List.map updater
        -- (\update matrix -> update matrix) == (<|)
        |> List.foldr (<|) matrix


revealAll : Grid -> Grid
revealAll =
    Matrix.map withStateRevealed


isZero : Location -> Grid -> Bool
isZero location matrix =
    getNumber location matrix == 0


get : Location -> Grid -> Maybe Cell
get =
    Matrix.get


getNumber : Location -> Grid -> Int
getNumber location matrix =
    getSurroundings location matrix
        |> List.filter (Tuple.second >> .mine)
        |> List.length


getSurroundings : Location -> Matrix a -> List ( Location, a )
getSurroundings ( row, column ) matrix =
    let
        surroundingLocations : List Location
        surroundingLocations =
            [ ( row - 1, column - 1 )
            , ( row - 1, column )
            , ( row - 1, column + 1 )
            , ( row, column - 1 )
            , ( row, column + 1 )
            , ( row + 1, column - 1 )
            , ( row + 1, column )
            , ( row + 1, column + 1 )
            ]

        helper : Location -> Maybe ( Location, a )
        helper location =
            Matrix.get location matrix
                |> Maybe.map ((,) location)
    in
    surroundingLocations
        |> List.map helper
        |> MaybeExtra.values


toListWithLocation : Matrix a -> List (List ( Matrix.Location, a ))
toListWithLocation grid =
    grid
        |> Matrix.mapWithLocation (,)
        |> Matrix.toList
