module Utils exposing (..)


when : (a -> Bool) -> (a -> a) -> a -> a
when shouldApply function value =
    if shouldApply value then
        function value
    else
        value


transform : (a -> b) -> (b -> b) -> (b -> a) -> a -> a
transform map change mapBack =
    map >> change >> mapBack


mapProperty : (a -> b) -> (b -> b) -> (b -> a -> a) -> a -> a
mapProperty get map set value =
    (get >> map >> set) value value


toIntOr0 : String -> Int
toIntOr0 string =
    String.toInt string
        |> Result.toMaybe
        |> Maybe.withDefault 0
