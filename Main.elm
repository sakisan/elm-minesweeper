module Main exposing (main)

import Html exposing (Html, program, span, table, td, text, tr)
import Html.Attributes exposing (value)
import Html.Events exposing (onClick, onInput)
import Minesweeper exposing (Cell, CellState(..), Config, Grid, Location)
import MouseEvent exposing (rightClick)
import Random as Random
import Svg exposing (Svg)
import Svg.Attributes as SA
import Utils exposing (mapProperty, toIntOr0, when)


type alias Model =
    { grid : Grid
    , config : Config
    , nextConfig : Config
    }


type Msg
    = Noop
    | Click Location
    | RightClick Location
    | Generate (List ( Int, Int ))
    | NewGame
    | SetNumberOfMines String
    | SetNumberOfRows String
    | SetNumberOfColumns String


main : Program Never Model Msg
main =
    program
        { init = init initialConfig
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


init : Config -> ( Model, Cmd Msg )
init config =
    ( { grid = Minesweeper.square 8
      , config = config
      , nextConfig = config
      }
    , generate config
    )


initialConfig : Config
initialConfig =
    { rows = 8
    , columns = 8
    , mines = 2
    }


generate : Config -> Cmd Msg
generate config =
    Random.pair
        (Random.int 0 (config.rows - 1))
        (Random.int 0 (config.columns - 1))
        |> Random.list config.mines
        |> Random.generate Generate


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    let
        thatsIt : Model -> ( Model, Cmd Msg )
        thatsIt newModel =
            ( newModel, Cmd.none )
    in
    case msg of
        Noop ->
            model
                |> thatsIt

        Click location ->
            model
                |> mapGrid (Minesweeper.revealIfUntouched location)
                |> thatsIt

        RightClick location ->
            model
                |> mapGrid (Minesweeper.mark location)
                |> thatsIt

        Generate random ->
            model
                |> withGrid (Minesweeper.newGrid model.config random)
                |> thatsIt

        NewGame ->
            init model.nextConfig

        SetNumberOfMines string ->
            model
                |> mapNextConfig (\c -> { c | mines = toIntOr0 string })
                |> thatsIt

        SetNumberOfRows string ->
            model
                |> mapNextConfig (\c -> { c | rows = toIntOr0 string })
                |> thatsIt

        SetNumberOfColumns string ->
            model
                |> mapNextConfig (\c -> { c | columns = toIntOr0 string })
                |> thatsIt


withGrid : Grid -> Model -> Model
withGrid grid model =
    { model | grid = grid }


mapGrid : (Grid -> Grid) -> Model -> Model
mapGrid map =
    mapProperty .grid map withGrid


withNextConfig : Config -> Model -> Model
withNextConfig config model =
    { model | nextConfig = config }


mapNextConfig : (Config -> Config) -> Model -> Model
mapNextConfig map =
    mapProperty .nextConfig map withNextConfig


subscriptions : Model -> Sub Msg
subscriptions =
    always Sub.none


squareSize : Int
squareSize =
    50


view : Model -> Html Msg
view actualModel =
    let
        model : Model
        model =
            { actualModel
                | grid =
                    actualModel.grid
                        |> when Minesweeper.isLost Minesweeper.revealAll
            }

        width : String
        width =
            (model.config.columns * squareSize)
                |> toString

        height : String
        height =
            (model.config.rows * squareSize)
                |> toString
    in
    Html.div []
        [ Svg.svg
            [ SA.id "game"
            , SA.width width
            , SA.height height
            , SA.viewBox ("0 0 " ++ width ++ " " ++ height)
            ]
            (svgView model)
        , gameInfo model
        ]


svgView : Model -> List (Svg Msg)
svgView model =
    let
        viewRow : Grid -> List ( Location, Cell ) -> List (Svg Msg)
        viewRow matrix row =
            List.map (viewCell matrix) row
    in
    model.grid
        |> Minesweeper.toListWithLocation
        |> List.map (viewRow model.grid)
        |> List.concat


viewCell : Grid -> ( Location, Cell ) -> Svg Msg
viewCell matrix ( ( row, column ) as location, cell ) =
    Svg.g
        [ Click location |> onClick
        , RightClick location |> always |> rightClick
        ]
        [ Svg.rect
            [ SA.x <| toString <| (column * squareSize)
            , SA.y <| toString <| (row * squareSize)
            , SA.width <| toString <| squareSize
            , SA.height <| toString <| squareSize
            , SA.fill <| squareColor cell.state
            , SA.fillOpacity "1"
            , SA.stroke "#000000"
            , SA.strokeWidth "1"
            ]
            []
        , Svg.text_
            [ SA.fontSize <| toString <| (squareSize // 2)
            , SA.x <| toString <| (column * squareSize + (squareSize // 3))
            , SA.y <| toString <| (row * squareSize + (squareSize - (squareSize // 3)))
            ]
            [ Svg.text <| squareText matrix location cell ]
        ]


squareColor : CellState -> String
squareColor state =
    case state of
        Untouched ->
            "#ffffff"

        Flag ->
            "#6C1000"

        Question ->
            "#FFD7AF"

        Revealed ->
            "#FAEBD7"


squareText : Grid -> Location -> Cell -> String
squareText matrix location cell =
    case cell.state of
        Untouched ->
            ""

        Minesweeper.Flag ->
            "X"

        Minesweeper.Question ->
            "?"

        Minesweeper.Revealed ->
            if cell.mine then
                "•"
            else
                Minesweeper.getNumber location matrix
                    |> toString
                    |> when ((==) "0") (always "")


gameInfo : Model -> Html Msg
gameInfo model =
    Html.div []
        [ if Minesweeper.isLost model.grid then
            Html.p [] [ text "You lost :(" ]
          else if Minesweeper.isWon model.grid then
            Html.p [] [ text "You won !!!" ]
          else
            Html.div []
                [ Html.p []
                    [ "Number of mines: "
                        ++ (toString <| model.config.mines)
                        |> text
                    ]
                , Html.p []
                    [ "Number of flags: "
                        ++ (toString <| Minesweeper.numberOfFlags model.grid)
                        |> text
                    ]
                ]
        , newGameButton model
        ]


newGameButton : Model -> Html Msg
newGameButton model =
    Html.div []
        [ Html.p []
            [ Html.button [ onClick NewGame ] [ text "new game" ]
            ]
        , Html.p []
            [ Html.span [] [ text "Number of mines: " ]
            , Html.input
                [ onInput SetNumberOfMines
                , value <| toString <| model.nextConfig.mines
                ]
                []
            ]
        , Html.p []
            [ Html.span [] [ text "Number of rows: " ]
            , Html.input
                [ onInput SetNumberOfRows
                , value <| toString <| model.nextConfig.rows
                ]
                []
            ]
        , Html.p []
            [ Html.span [] [ text "Number of columns: " ]
            , Html.input
                [ onInput SetNumberOfColumns
                , value <| toString <| model.nextConfig.columns
                ]
                []
            ]
        ]


textView : Model -> Html Msg
textView model =
    let
        tableViewRow : Grid -> List ( Location, Cell ) -> Html Msg
        tableViewRow matrix row =
            row
                |> List.map (tableViewCell matrix)
                |> tr []

        tableViewCell : Grid -> ( Location, Cell ) -> Html Msg
        tableViewCell matrix cell =
            textViewCell matrix cell
                |> td []
    in
    model.grid
        |> Minesweeper.toListWithLocation
        |> List.map (tableViewRow model.grid)
        |> table []


textViewCell : Grid -> ( Location, Cell ) -> List (Html Msg)
textViewCell matrix (( location, _ ) as tuple) =
    [ span
        [ Click location |> onClick
        , RightClick location |> always |> rightClick
        ]
        [ textViewState matrix tuple |> text
        ]
    ]


textViewState : Grid -> ( Location, Cell ) -> String
textViewState matrix ( location, cell ) =
    case cell.state of
        Untouched ->
            "□"

        Flag ->
            "X"

        Question ->
            "?"

        Revealed ->
            if cell.mine then
                "•"
            else
                Minesweeper.getNumber location matrix
                    |> toString
                    |> when ((==) "0") (\_ -> "_")
